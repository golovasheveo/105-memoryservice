package telran.util.memory;

@SuppressWarnings(value = "ALL")
public class MemoryService {
    public static int getAvailableMemoryBlockSize ( ) {
        int size = Integer.MAX_VALUE;
        while (true) {
            try {
                byte[] ar = new byte[ size ];
                return size;
            } catch ( OutOfMemoryError e ) {
                size = size - 1;
            }
        }

    }



    public static int getAvailableMemoryBlockSize ( boolean fast ) {
        int middle = 0;
        int left = 0;
        int right = Integer.MAX_VALUE;
        int size = 0;

        try {
            byte[] ar = new byte[ right ];
            ar = null;
            return right;

        } catch ( OutOfMemoryError error ) {

            while (left <= right) {
                try {
                    middle = (int) ( (long) left + (long) right ) / 2;
                    byte[] ar = new byte[ middle ];

                    ar = null;
                    size = middle;
                    left = middle + 1;
                } catch (OutOfMemoryError e) {
                    right = middle - 1;
                }
            }
            return size;
        }
    }
}